#ifndef PIANOKEYBOARD_H
#define PIANOKEYBOARD_H

#include <QtGui/QGraphicsView>
#include <QKeyEvent>
#include <vector>
#include <map>

#include "pianokey.hpp"

// PianoKeyboard
// Qt widget of piano keyboard with appropriate slots and signals
class PianoKeyboard : public QGraphicsView
{
    Q_OBJECT

public:
    PianoKeyboard();
    ~PianoKeyboard();
    static bool isWhite(int);

    // disable scrolling
    void scrollContentsBy(int, int){}

public slots:
    void getNoteOn(int value);
    void getNoteOff(int value);
    void setNoteOn(int value);
    void setNoteOff(int value);

signals:
    void noteOn(int value);
    void noteOff(int value);
    void setSustainPedal(bool value);

protected:
    void resizeEvent(QResizeEvent* event);
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);

private:
    QGraphicsScene* scene;
    std::vector<PianoKey*> keys;
    static const std::map<int,int> keyMapping;

    // constant assigned to sustain
    static const int Sus = 4242;
};

#endif // PIANOKEYBOARD_H
