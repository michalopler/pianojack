#ifndef GAMEINFO_HPP
#define GAMEINFO_HPP

#include <QString>
#include "ui_gameinfo.h"

// GameInfo
// widget displaying info and buttons over main game screen
class GameInfo : public QWidget
{
    Q_OBJECT

public:
    GameInfo (QWidget *parent = 0);
    void gameMode();
    void songLoaded(QString name);
    void displayEnd();
    void displayInfo(QString main, QString info);
    void setActive(bool a) {active = a; ui.startButton->setEnabled(active);}
    void setPaused(bool p);
    void addScore(int value);

private slots:
    void pauseButtonPressed();

signals:
    void pauseGame();
    void unpauseGame();
    void startGame();
    void settings();
    void loadSong();

private:
    Ui::GameInfo ui;

    bool active;
    bool paused;
    int score;
};

#endif // GAMEINFO_HPP
