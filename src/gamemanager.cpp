#include <QApplication>
#include <QMessageBox>
#include <QMenuBar>
#include <QFileDialog>
#include <QSettings>
#include <string>
#include "partpicker.hpp"
#include "gamemanager.hpp"

GameManager::GameManager() : midi(this), inChannel(0)
{
    layout = new QVBoxLayout();
    keyboard = new PianoKeyboard();
    window = new QWidget();
    gameWidget = new GameView();
    setDialog = new SettingsDialog();

    gameWidget->setFocusProxy(keyboard);
    layout->addWidget(gameWidget);
    layout->setStretchFactor(gameWidget,3);
    layout->addWidget(keyboard);
    layout->setStretchFactor(keyboard,1);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);
    layout->setAlignment(Qt::AlignBottom);

    window->setLayout(layout);
    setCentralWidget(window);
    readSettings();

    keyboard->setFocus();
    connectSignals();
}

GameManager::~GameManager()
{
    delete setDialog;
    delete gameWidget;
    delete keyboard;
    delete layout;
    delete window;
}

// connect all the mighty widgets
void GameManager::connectSignals()
{
    connect(keyboard, SIGNAL(noteOn(int)), this, SLOT(noteOn(int)));
    connect(keyboard, SIGNAL(noteOff(int)), this, SLOT(noteOff(int)));

    connect(keyboard, SIGNAL(noteOn(int)), gameWidget, SLOT(noteOn(int)));
    connect(keyboard, SIGNAL(noteOff(int)), gameWidget, SLOT(noteOff(int)));

    connect(keyboard, SIGNAL(setSustainPedal(bool)), this, SLOT(sustainPedal(bool)));

    // these signals are emited from JACK callbacks - using asynchronous queued connection
    connect(this, SIGNAL(midiNoteOn(int)), keyboard, SLOT(setNoteOn(int)), Qt::QueuedConnection);
    connect(this, SIGNAL(midiNoteOff(int)), keyboard, SLOT(setNoteOff(int)), Qt::QueuedConnection);

    connect(this, SIGNAL(midiNoteOn(int)), gameWidget, SLOT(noteOn(int)), Qt::QueuedConnection);
    connect(this, SIGNAL(midiNoteOff(int)), gameWidget, SLOT(noteOff(int)), Qt::QueuedConnection);

    // this signals calls error handling function asynchronously from JACK callbacks
    connect(this, SIGNAL(raiseError(QString)), this, SLOT(endWithError(QString)), Qt::QueuedConnection);

    // controls from game view
    connect(gameWidget, SIGNAL(settings()), this, SLOT(settings()));
    connect(gameWidget, SIGNAL(openFile()), this, SLOT(fileOpen()));
}

// show app and activate midi client
void GameManager::activate()
{
    midi.activate();
    show();
}

// show open file dialog
void GameManager::fileOpen()
{
    gameWidget->pauseGame();
    QString fn = QFileDialog::getOpenFileName(this, tr("Open File..."),
        QString(), tr("Music XML Files (*.xml)"));
    if (!fn.isEmpty())
        load(fn);
}

// open file and load part
void GameManager::load(const QString& name)
{

    // load xml parts
    try
    {
        XmlLoader xml(name);
        QList<QString> parts;
        xml.getParts(parts);

        // let user pick part and tempo
        PartPicker p;
        p.setParts(parts);
        p.exec();

        std::vector<XmlNote> notes;

        // read note from xml data
        xml.readPart(p.getPart(), 60000/p.getTempo(), notes);

        // clear gameview
        gameWidget->clear();

        // set new song
        gameWidget->setSong(notes, name);
    }
    catch(std::invalid_argument e) {
         QMessageBox::critical(this, tr("File couldn't be opened"), tr("Error during XML parsing. Maybe wrong format?"));
    }
}

// displays settings window
void GameManager::settings()
{
    gameWidget->pauseGame();
    auto inCon = midi.getConnectedInPorts();
    auto outCon = midi.getConnectedOutPorts();

    // set dialog values
    setDialog->setTranspose(transpose);
    setDialog->setVelocity(velocity);
    setDialog->setSpeed(speed);
    setDialog->setVisibility(gameVisible);
    setDialog->setInPorts(midi.getInPorts(),
        inCon.size() == 0 ? "none" : inCon[0]);
    setDialog->setOutPorts(midi.getOutPorts(),
       outCon.size() == 0 ? "none" : outCon[0]);

    // show dialog
    setDialog->exec();

    // get dialog values
    velocity = setDialog->getVelocity();
    transpose = setDialog->getTranspose();
    speed = setDialog->getSpeed();
    gameVisible = setDialog->getVisibility();

    // reconnect ports
    midi.disconnectInPort();
    midi.disconnectOutPort();
    midi.connectInPort(setDialog->getInPort());
    midi.connectOutPort(setDialog->getOutPort());
    midi.setVelocity(velocity);

    // clears game view
    gameWidget->clear();

    gameWidget->setVisible(gameVisible);
    gameWidget->setSpeed(speed);
}

// load settings from Qt
void GameManager::readSettings()
{
    QSettings settings("PianoJACK", "PianoJACK");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 150)).toSize();

    velocity = settings.value("velocity", 64).toInt();
    midi.setVelocity(velocity);

    transpose = settings.value("transpose", 0).toInt();

    speed = settings.value("speed", 3).toInt();
    gameWidget->setSpeed(speed);

    gameVisible = true;

    resize(size);
    move(pos);
}

// save settings to Qt
void GameManager::writeSettings()
{
    QSettings settings("PianoJACK", "PianoJACK");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
    settings.setValue("velocity", velocity);
    settings.setValue("transpose", transpose);
    settings.setValue("speed", speed);
}

// save settings before closing
void GameManager::closeEvent(QCloseEvent *event)
 {
    writeSettings();
    event->accept();
 }

// displays simple about message
void GameManager::about()
 {
    QMessageBox::about(this, tr("About Application"),
             tr("<b>PianoJACK</b> is simple piano playing game "
                "built around JACK audio server."));
 }

// slot just redirects MIDI data to MidiHandler
// some game related implementation to be added later
void GameManager::noteOn(int value)
{
    midi.noteStart((midi_data) transposeNote(value));
}

// slot just redirects MIDI data to MidiHandler
// some game related implementation to be added later
void GameManager::noteOff(int value)
{
    midi.noteEnd((midi_data) transposeNote(value));
}

// slot for receiving sustain pedal data
void GameManager::sustainPedal(bool value)
{
    if(value) midi.addSustainDown();
    else midi.addSustainUp();
}

// slot for bad things
void GameManager::endWithError(QString error)
{
    QMessageBox::critical(this, tr("JACK server error"),error);
    QApplication::quit();
}


// jack error callback
void GameManager::jackError(std::string error)
{
    emit raiseError(QString(error.c_str()));
}

// transpose note
int GameManager::transposeNote(int value)
{
    if(value >= 127 - transpose) return 127;
    else if(value < transpose) return 0;
    else return value + transpose;
}

// callback function for proccessing MIDI in events
bool GameManager::processMidiInEvent(MidiEvent& event)
{
    // play only the appropriate channel
    if((event.data[0] & 0xf) != inChannel) return false;

    // test event type
    switch(event.data[0] & 0xf0)
    {
    case 0x90:
        emit midiNoteOn(event.data[1]);
        event.data[1] = transposeNote(event.data[1]);
        return true;
    case 0x80:
        emit midiNoteOff(event.data[1]);
        event.data[1] = transposeNote(event.data[1]);
        return true;
    case 0xB0:
        // sustain pedal event
        if(event.data[1] == 0x40)
        {
            return true;
        }
        else return false;
    default:
        return false;
    }
}
