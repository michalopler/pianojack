#ifndef PIANOKEY_HPP
#define PIANOKEY_HPP

#include <QGraphicsObject>

// PianoKey
// Implementation of piano key as Qt graphics object. This is abstract class
// from which WhitePianoKey and BlackPianoKey inherit
class PianoKey : public QGraphicsObject
{
    Q_OBJECT

public:
    PianoKey(int, QGraphicsScene*);
    virtual QRectF boundingRect() const = 0;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget) = 0;

    void drawPress();
    void drawRelease();

signals:
    void pressed(int);
    void released(int);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);

    int value;
    bool down;
    QGraphicsScene* scene;
};

// WhitePianoKey
// Inherits PianoKey and adds own paint() and boundingRect() methods
class WhitePianoKey : public PianoKey
{
    Q_OBJECT

public:
    WhitePianoKey(int, QGraphicsScene*);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget);

    static const int width = 20;
    static const int height = 120;
};

// WhitePianoKey
// Inherits PianoKey and adds own paint() and boundingRect() methods
class BlackPianoKey : public PianoKey
{
    Q_OBJECT

public:
    BlackPianoKey(int, QGraphicsScene*);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget);

    static const int width = 10;
    static const int height = 75;
};




#endif // PIANOKEY_HPP
