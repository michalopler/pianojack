#include <QDebug>

#include "pianokeyboard.hpp"

PianoKeyboard::PianoKeyboard()
{
    scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::BspTreeIndex);
    scene->setSceneRect(0, 0, 52 * WhitePianoKey::width, WhitePianoKey::height);
    setScene(scene);
    fitInView(2,2,52 * WhitePianoKey::width - 4, WhitePianoKey::height - 4);

    setRenderHint(QPainter::Antialiasing);
    setMinimumWidth(52 * WhitePianoKey::width);
    setMaximumHeight(200);

    setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );

    int pos = 0;
    for(int i = 21; i <= 108; i++) {
        PianoKey* key;
        if(isWhite(i%12)) {
            key = new WhitePianoKey(i, scene);
            key->setPos(pos,0);
            scene->addItem(key);

            pos += WhitePianoKey::width;
            keys.push_back(key);
        }
        else {
            key = new BlackPianoKey(i, scene);
            key->setPos(pos - BlackPianoKey::width/2,0);
            scene->addItem(key);
            keys.push_back(key);
        }
        QObject::connect(key, SIGNAL(pressed(int)),
                         this, SLOT(getNoteOn(int)));
        QObject::connect(key, SIGNAL(released(int)),
                         this, SLOT(getNoteOff(int)));
    }

}

PianoKeyboard::~PianoKeyboard()
{
    for(unsigned int i = 0; i < keys.size(); ++i) delete keys[i];
    delete scene;
}

void PianoKeyboard::getNoteOn(int value)
{
    emit noteOn(value);
}

void PianoKeyboard::getNoteOff(int value)
{
    emit noteOff(value);
}

void PianoKeyboard::setNoteOn(int value)
{
    if(value >= 21 && value <= 108) keys[value - 21]->drawPress();
}

void PianoKeyboard::setNoteOff(int value)
{
    if(value >= 21 && value <= 108) keys[value - 21]->drawRelease();
}

void PianoKeyboard::resizeEvent(QResizeEvent* event)
{
    fitInView(2,2,52 * WhitePianoKey::width - 4, WhitePianoKey::height - 4);
    QGraphicsView::resizeEvent(event);
}

void PianoKeyboard::keyPressEvent(QKeyEvent *event)
{

    if(event->isAutoRepeat()) return;
    auto valueIt = keyMapping.find(event->key());
    if(valueIt != keyMapping.end())
    {
        if(valueIt->second == Sus)
        {
            emit setSustainPedal(true);
        }
        else {
            setNoteOn(valueIt->second);
            emit noteOn(valueIt->second);
        }
    }
}

void PianoKeyboard::keyReleaseEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat()) return;
    auto valueIt = keyMapping.find(event->key());
    if(valueIt != keyMapping.end())
    {
        if(valueIt->second == Sus)
        {
            emit setSustainPedal(false);
        }
        else {
            setNoteOff(valueIt->second);
            emit noteOff(valueIt->second);
        }
    }
}

// tests if value represents white key
bool PianoKeyboard::isWhite(int i) {
    switch(i) {
    case 0:
    case 2:
    case 4:
    case 5:
    case 7:
    case 9:
    case 11:
        return true;
    default:
        return false;
    }
}

// key mappings for events
const std::map<int,int> PianoKeyboard::keyMapping = {
    {Qt::Key_Q, 48},        // Q - C
    {Qt::Key_2, 49},        // 2 - Cis
    {Qt::Key_W, 50},        // W - D
    {Qt::Key_3, 51},        // 3 - Dis
    {Qt::Key_E, 52},        // E - E
    {Qt::Key_R, 53},        // R - F
    {Qt::Key_5, 54},        // 5 - Fis
    {Qt::Key_T, 55},        // T - G
    {Qt::Key_6, 56},        // 6 - Gis
    {Qt::Key_Y, 57},        // Y - A
    {Qt::Key_7, 58},        // 7 - Ais
    {Qt::Key_U, 59},        // U - H
    {Qt::Key_I, 60},        // I - C
    {Qt::Key_C, 60},        // C - C
    {Qt::Key_F, 61},        // F - Cis
    {Qt::Key_V, 62},        // V - D
    {Qt::Key_G, 63},        // G - Dis
    {Qt::Key_B, 64},        // B - E
    {Qt::Key_N, 65},        // N - F
    {Qt::Key_J, 66},        // J - Fis
    {Qt::Key_M, 67},        // M - G
    {Qt::Key_K, 68},        // K - Gis
    {Qt::Key_Comma, 69},    // , - A
    {Qt::Key_L, 70},        // L - Ais
    {Qt::Key_Period, 71},   // . - H
    {Qt::Key_Slash, 72},    // / - C
    {Qt::Key_Space, 4242}   // sustain
};
