#include <QApplication>

#include "pianokeyboard.hpp"
#include "gamemanager.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // activate main widget
    GameManager gm;
    gm.activate();
    return app.exec();
}
