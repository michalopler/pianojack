#ifndef MIDIHANDLER_HPP
#define MIDIHANDLER_HPP

#include <vector>
#include <string>

extern "C" {
#include <jack/jack.h>
#include <jack/midiport.h>
}

#include "ringbuffer.hpp"
#include "midievent.hpp"

typedef unsigned char midi_data;

// MidiClass
// Parent object of MidiHandler should inherit this in order to receive
// callbacks
class MidiClass {
public:
    virtual bool processMidiInEvent(MidiEvent&) = 0;
    virtual void jackError(std::string) = 0;
};

// MidiHandler
// C++ wrapper around JACK Audio server
class MidiHandler {
public:
    MidiHandler(MidiClass* _parent);
    ~MidiHandler();

	jack_nframes_t getFrameTime() { return jack_frame_time(client);}	
	void activate();

    std::vector<std::string> getInPorts();
    std::vector<std::string> getOutPorts();

    std::vector<std::string> getConnectedInPorts();
    std::vector<std::string> getConnectedOutPorts();

    bool connectInPort(const std::string&);
    bool connectOutPort(const std::string&);
    void disconnectInPort() { jack_port_disconnect(client,inPort);}
    void disconnectOutPort() { jack_port_disconnect(client,outPort);}

    static int staticProcess(jack_nframes_t, void*);
    static void staticShutDown(void*);

	void addEvent(MidiEvent e) { outBuffer.write(e); }
	void noteStart(midi_data n) { 
		addEvent(MidiEvent(getFrameTime(), 0x90, n, velocity)); }
	void noteEnd(midi_data n)  {
		addEvent(MidiEvent(getFrameTime(), 0x80, n, velocity)); }
    void addSustainDown() {
        addEvent(MidiEvent(getFrameTime(), 0xB0, 0x40, 120)); }
    void addSustainUp() {
        addEvent(MidiEvent(getFrameTime(), 0xB0, 0x40, 20)); }

	
	void setVelocity(midi_data v) {velocity = v;}

private:
	int process(jack_nframes_t);

	RingBuffer<MidiEvent> outBuffer;
	jack_client_t* client;
	jack_port_t* outPort;
    jack_port_t* inPort;

    MidiClass* parent;
	midi_data velocity;
};


#endif
