#ifndef MIDIEVENT_HPP
#define MIDIEVENT_HPP

extern "C" {
#include <jack/midiport.h>
}

// MidiEvent class - a simple container for MIDI events
class MidiEvent
{
public:
	MidiEvent(){}
    MidiEvent(jack_nframes_t inFrame, jack_midi_data_t* inData): frame(inFrame),size(3) {
		memcpy( data, inData, sizeof(char) *3 );
	}
    MidiEvent(jack_midi_event_t e): frame(e.time), size(e.size) {
        memcpy( data, e.buffer, sizeof(char) *size );
    }
    MidiEvent(jack_nframes_t inFrame, unsigned char e1, unsigned char e2, unsigned char e3):frame(inFrame), size(3) {
		data[0] = e1;
		data[1] = e2;
		data[2] = e3;
	}

	jack_nframes_t frame;
    size_t size;
	jack_midi_data_t data[3];
};
#endif
