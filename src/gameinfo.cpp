#include "gameinfo.hpp"
#include <QFileInfo>

GameInfo::GameInfo(QWidget *parent)
    : QWidget(parent), active(false),
    paused(false), score(0)
{
    ui.setupUi(this);
    ui.scoreLabel->setVisible(false);
    ui.scoreValue->setVisible(false);
    ui.pauseButton->setVisible(false);

    connect(ui.settingsButton, SIGNAL(clicked()), this, SIGNAL(settings()));
    connect(ui.openButton, SIGNAL(clicked()), this, SIGNAL(loadSong()));
    connect(ui.startButton, SIGNAL(clicked()), this, SIGNAL(startGame()));
    connect(ui.pauseButton, SIGNAL(clicked()), this, SLOT(pauseButtonPressed()));
}

// sets widgets visible during game time
void GameInfo::gameMode()
{
    ui.infoLabel->setVisible(false);
    ui.mainLabel->setVisible(false);
    ui.openButton->setVisible(false);
    ui.startButton->setVisible(false);

    ui.scoreLabel->setVisible(true);
    ui.scoreValue->setVisible(true);
    ui.pauseButton->setVisible(true);
}

// displays song loaded info screen
void GameInfo::songLoaded(QString name)
{
    ui.infoLabel->setText("Song " + QFileInfo(name).fileName() + " successfully loaded.");
    ui.mainLabel->setText("Are you ready to play?");
    ui.startButton->setText("Play");

    score = 0;
    ui.scoreValue->setText(QString::number(0));

    setActive(true);
    setPaused(false);
}

// changes info messages on screen
void GameInfo::displayInfo(QString main, QString info)
{
    ui.scoreLabel->setVisible(false);
    ui.scoreValue->setVisible(false);
    ui.pauseButton->setVisible(false);

    ui.infoLabel->setVisible(true);
    ui.mainLabel->setVisible(true);
    ui.openButton->setVisible(true);
    ui.startButton->setVisible(true);

    ui.mainLabel->setText(main);
    ui.infoLabel->setText(info);
}

// displays game end screen
void GameInfo::displayEnd()
{
    ui.startButton->setText("Play Again");
    displayInfo("Well done! You scored " + QString::number(score) + " points.",
                "Play again or load another song.");
    score = 0;
    ui.scoreValue->setText(QString::number(0));
}

// refreshes score board
void GameInfo::addScore(int value)
{
    score += value;
    ui.scoreValue->setText(QString::number(score));
}

// receives pause button press and handles the action
void GameInfo::pauseButtonPressed()
{
    if(!paused)
    {
        ui.pauseButton->setIcon(QIcon(":/images/play.png"));
        displayInfo("Game paused", "Continue, restart or load another song.");
        ui.pauseButton->setVisible(true);
        ui.scoreLabel->setVisible(true);
        ui.scoreValue->setVisible(true);
        ui.startButton->setText("Restart");
        emit pauseGame();
    }
    else
    {
        ui.pauseButton->setIcon(QIcon(":/images/pause.png"));
        emit unpauseGame();
    }
    paused = !paused;
}

// set paused and icons
void GameInfo::setPaused(bool p)
{
    if(!p) ui.pauseButton->setIcon(QIcon(":/images/pause.png"));
    else ui.pauseButton->setIcon(QIcon(":/images/play.png"));
    paused = p;
}
