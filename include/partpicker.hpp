#ifndef PARTPICKER_HPP
#define PARTPICKER_HPP

#include <QList>
#include <QString>
#include "ui_partpicker.h"

// PartPicker
// QDialog to select part and tempo, displayed after file open.
class PartPicker : public QDialog
{
    Q_OBJECT

public:
    PartPicker (QWidget *parent = 0);
    void setParts(const QList<QString>& parts);
    QString getPart();
    int getTempo();

private:
    Ui::PartPicker ui;
};

#endif // PARTPICKER_HPP
