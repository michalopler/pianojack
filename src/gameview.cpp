#include "gameview.hpp"
#include "pianokeyboard.hpp"
#include <QDebug>
#include <QFont>
#include <QLabel>

GameView::GameView()
    : speed(3), frmrt(30), position(0), endPosition(0),
     paused(true), active(false),
     streak(0)
{
    scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setScene(scene);
    setSceneRect(0, -height(), 52*WhitePianoKey::width, height());
    fitInView(0, -height(),52*WhitePianoKey::width, height());

    gameInfo = new GameInfo(this);
    gameInfo->setFocusProxy(focusProxy());

    setRenderHint(QPainter::Antialiasing);
    setMinimumHeight(200);
    setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff);

    scene->setBackgroundBrush(QColor(100,100,100));

    // set timer, but launching only swhen the game starts
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateNotes()));
    timer->setInterval(frmrt);

    connect(gameInfo, SIGNAL(loadSong()),this, SIGNAL(openFile()));
    connect(gameInfo, SIGNAL(settings()),this, SIGNAL(settings()));
    connect(gameInfo, SIGNAL(startGame()),this, SLOT(startGame()));

    connect(gameInfo, SIGNAL(pauseGame()),this, SLOT(pauseGame()));
    connect(gameInfo, SIGNAL(unpauseGame()),this, SLOT(unpauseGame()));

}

GameView::~GameView()
{
    delete gameInfo;
    delete scene;
    delete timer;
}

// set Notes
void GameView::setSong(const std::vector<XmlNote> & notes, QString name)
{
    for(auto i = notes.begin(); i != notes.end(); ++i)
    {
        addNote(i->val, i->begin, i->end);
    }
    active = true;

    // prepare game info view for start
    gameInfo->songLoaded(name);
}

// clears view
void GameView::clear(QString main, QString info)
{
    gameInfo->setActive(false);
    gameInfo->displayInfo(main, info);

    timer->stop();
    reset();
    endPosition = 0;

    // clean up after ourselves
    for(int i = 0; i < 88; i++)
    {
        for(auto n : notes[i])
        {
            delete n;
        }
        notes[i].clear();
    }

}

// resets game
void GameView::reset()
{
    setSceneRect(0, -height(), 52*WhitePianoKey::width, height());
    fitInView(0, -height(),52*WhitePianoKey::width, height());
    position = 0;
    streak = 0;

    // reset note blocks
    for(int i = 0; i < 88; i++) notesId[i] = 0;
    for(int i = 0; i < 88; i++)
        for(auto n : notes[i])
            n->changeState(0);
}

// public slot which receives note down event
void GameView::noteOn(int value)
{
    if(!active) return;
    // find first note above keyboard
    while(notesId[value - 21] < notes[value-21].size() &&
        -notes[value-21][notesId[value - 21]]->y() < position - 15 )
            notesId[value - 21]++;

    if(notes[value - 21].size() != notesId[value-21])
    {
        // distance from position
        int dist = abs(notes[value-21][notesId[value - 21]]->y() + notes[value-21][notesId[value-21]]->height() - 3 + position);

        if(dist < 15)
        {
            // success
            gameInfo->addScore((15 - dist)*2);
            streak++;
            notes[value-21][notesId[value-21]]->changeState(1);

            if(streak > 5 && streak%5 == 0) gameInfo->addScore(5*streak);
        }
        else
        {
            // wrong hit
            if(dist < 50) notes[value-21][notesId[value-21]]->changeState(2);

            gameInfo->addScore(-10);

            streak = 0;
        }
    }
}

// public slot which receives note up event
void GameView::noteOff(int value)
{
    if(!active) return;
    if(notesId[value-21] >= notes[value - 21].size()) return;
    if(notes[value - 21][notesId[value-21]]->getState() == 1)
    {
        notes[value-21][notesId[value-21]]->changeState(0);
        notesId[value-21]++;
    }
}

// game control functions
void GameView::unpauseGame()
{
    if(!active) return;
    gameInfo->gameMode();

    timer->start();
    paused = false;
}

void GameView::pauseGame()
{
    timer->stop();
    paused = true;
}

void GameView::startGame()
{
    reset();
    gameInfo->setPaused(false);
    unpauseGame();
}

// adding note to game
void GameView::addNote(int value, int start, int end)
{
    NoteBlock* note = new NoteBlock(getNotePos(value + 1) - getNotePos(value),(end-start)*speed/frmrt);
    note->setPos(getNotePos(value), -end*speed/frmrt - height());
    scene->addItem(note);
    notes[value - 21].push_back(note);

    // adjust end position
    if(end*speed/frmrt + height() > endPosition)
        endPosition = end*speed/frmrt + height();
}

// animation core, just moves frame
void GameView::updateNotes()
{
    setSceneRect(0, -position - height(),52*WhitePianoKey::width, height());
    position += speed;

    if(position > endPosition + 10)
    {
        reset();
        timer->stop();
        gameInfo->displayEnd();
    }
}

// change view when resized
void GameView::resizeEvent(QResizeEvent* event)
{
    fitInView(0, -position - height(),52*WhitePianoKey::width, height());

    // resize info
    gameInfo->resize(size());

    QGraphicsView::resizeEvent(event);
}


// ugly function which returns note position
int GameView::getNotePos(int value) {
    int pos = ((value - 21)/12)*WhitePianoKey::width*7;

    // special cases
    if(value == 21) return pos;
    if(value == 109) return pos + 3*WhitePianoKey::width;

    switch((value-21)%12) {
    case 0:     // A
        return pos + BlackPianoKey::width/2;
    case 1:     // Ais
        return pos + WhitePianoKey::width - BlackPianoKey::width/2;
    case 2:     // H
        return pos + WhitePianoKey::width + BlackPianoKey::width/2;
    case 3:     // C
        return pos + 2*WhitePianoKey::width;
    case 4:     // Cis
        return pos + 3*WhitePianoKey::width - BlackPianoKey::width/2;
    case 5:     // D
        return pos + 3*WhitePianoKey::width + BlackPianoKey::width/2;
    case 6:     // Dis
        return pos + 4*WhitePianoKey::width - BlackPianoKey::width/2;
    case 7:     // E
        return pos + 4*WhitePianoKey::width + BlackPianoKey::width/2;
    case 8:     // F
        return pos + 5*WhitePianoKey::width;
    case 9:     // Fis
        return pos + 6*WhitePianoKey::width - BlackPianoKey::width/2;
    case 10:    // G
        return pos + 6*WhitePianoKey::width + BlackPianoKey::width/2;
    case 11:    // Gis
        return pos + 7*WhitePianoKey::width - BlackPianoKey::width/2;
    default:
        return 1;
    }
    return pos;
}
