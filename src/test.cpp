#include <thread>
#include <chrono>
#include <iostream>

#include "midihandler.hpp"
#include "midievent.hpp"

using namespace std;

int main() {
	MidiHandler mh;
	mh.activate();

	for(int i = 0; i < 50; i++) {
		mh.noteStart(40 + i);
		this_thread::sleep_for(chrono::milliseconds(500));
		mh.noteEnd(40 + i);
		//this_thread::sleep_for(chrono::seconds(1));
	}
	
	this_thread::sleep_for(chrono::seconds(1));

	return 0;
}
