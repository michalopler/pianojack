#ifndef GAMEVIEW_HPP
#define GAMEVIEW_HPP

#include <QtGui/QGraphicsView>
#include <QLabel>
#include <QTimer>
#include <vector>
#include <array>
#include "noteblock.hpp"
#include "musicxmlloader.hpp"
#include "gameinfo.hpp"

// GameView
// Qt widget of the falling notes view
class GameView : public QGraphicsView
{
    Q_OBJECT

public:
    GameView();
    ~GameView();
    void setSong(const std::vector<XmlNote>& notes, QString name);
    void clear(QString main = "Game cleared", QString info = "Please load song to start game");
    void reset();

    void setSpeed(int value) {speed = value;}

protected:
    void resizeEvent(QResizeEvent* event);

signals:
    void settings();
    void openFile();

private slots:
    void updateNotes();

public slots:
    void noteOn(int value);
    void noteOff(int value);
    void unpauseGame();
    void pauseGame();
    void startGame();

private:
    void addNote(int value, int start, int end);
    int getNotePos(int value);
    void addScore(int value, QString messsage);

    QGraphicsScene* scene;
    QTimer* timer;
    GameInfo* gameInfo;

    // number of pixels blocks move per one update
    int speed;
    int frmrt;
    int position;
    int endPosition;

    bool paused;
    bool active;
    int streak;

    std::array<std::vector<NoteBlock*>,88> notes;
    std::array<unsigned int,88> notesId;

};

#endif // GAMEVIEW_HPP
