#ifndef NOTEBLOCK_HPP
#define NOTEBLOCK_HPP

#include <QGraphicsObject>

// NoteBlock
// Represents the falling note block
class NoteBlock : public QGraphicsObject
{
    Q_OBJECT

public:
    NoteBlock(int, int);
    QRectF boundingRect() const;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
    void changeState(int val) { state = val;}
    int height() {return h;}
    int getState() {return state;}

private:
    int w;
    int h;
    int state;
};
#endif // NOTEBLOCK_HPP
