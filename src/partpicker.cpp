#include "partpicker.hpp"

PartPicker::PartPicker(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}

// really nothing interesting here
void PartPicker::setParts(const QList<QString> &parts)
{
    ui.partCombo->addItems(parts);
}

QString PartPicker::getPart()
{
    return ui.partCombo->currentText();
}

int PartPicker::getTempo()
{
    return ui.tempoSpin->value();
}
