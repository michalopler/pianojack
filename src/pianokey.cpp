#include <QPainter>
#include <QPoint>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneHoverEvent>

#include "pianokey.hpp"

PianoKey::PianoKey(int val, QGraphicsScene* s):
    value(val), down(false), scene(s) {}

// simple public methods, which draw change without emiting signals
// very useful when visualising input MIDI without creating infinite loop
void PianoKey::drawPress() {
    down = true;
    update();
}

void PianoKey::drawRelease() {
    down = false;
    update();
}

// MouseEvent handlers
void PianoKey::mousePressEvent(QGraphicsSceneMouseEvent*)
{
    if(down) return;
    drawPress();
    emit pressed(value);
}

void PianoKey::mouseReleaseEvent(QGraphicsSceneMouseEvent*)
{
    if(!down) return;
    drawRelease();
    emit released(value);
    this->ungrabMouse();
}

void PianoKey::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    // little hack to properly change active key when moving mouse
    QGraphicsItem* topItem = scene->itemAt(event->scenePos());
    if(dynamic_cast<PianoKey*>(topItem)==0) return;
    if (!(topItem == this)) {
        if(topItem != 0) {
            this->ungrabMouse();
            topItem->grabMouse();
            if(!down) return;
            emit released(value);
            drawRelease();
        }
    }
    else if(!down) {
        emit pressed(value);
        drawPress();
    }
}


BlackPianoKey::BlackPianoKey(int val, QGraphicsScene* s):
    PianoKey(val,s)
{
    // black keys are on top of white
    setZValue(2);
}

QRectF BlackPianoKey::boundingRect() const
{
    return QRectF(0,0,width,height);
}

// paint black key
void BlackPianoKey::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen;
    pen.setWidth(0.5);
    pen.setBrush(Qt::black);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter->setPen(Qt::NoPen);
    QBrush brush(Qt::black, Qt::SolidPattern);

    // different style when pushed
    if(down) {
        brush.setColor(QColor(114,159,207));
    }
    painter->setBrush(brush);
    painter->drawRect(0,0,width,height);
}


WhitePianoKey::WhitePianoKey(int val, QGraphicsScene* s):
    PianoKey(val,s)
{
    // white keys are below black
    setZValue(-1);
}

QRectF WhitePianoKey::boundingRect() const
{
    return QRectF(0,0,width,height);
}

// paint white key
void WhitePianoKey::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen;
    pen.setWidth(0.5);
    pen.setBrush(Qt::black);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    painter->setPen(Qt::NoPen);
    QBrush brush(Qt::transparent, Qt::SolidPattern);

    // different style when pushed
    if(down) {
        brush.setColor(QColor(114,159,207));
    }
    painter->setBrush(brush);
    painter->drawRect(0,0,width,height);

    // draws just left border line, because there is a full row of keys
    painter->setPen(pen);
    painter->drawLine(QPoint(0,0),QPoint(0,height));
}

