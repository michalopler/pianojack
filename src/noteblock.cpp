#include <QPainter>
#include <QDebug>
#include "noteblock.hpp"

NoteBlock::NoteBlock(int width, int height):
    w(width), h(height), state(0) {}

QRectF NoteBlock::boundingRect() const
{
    return QRectF(0,0,w,h);
}

// paint function override
void NoteBlock::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget*)
{
    QBrush brush(Qt::black, Qt::SolidPattern);

    if(state == 0) brush.setColor(QColor(239,229,108));
    else if(state == 1) brush.setColor(QColor(250,240,120));
    else brush.setColor(QColor(110,130,140));

    painter->setPen(Qt::black);
    painter->setBrush(brush);
    painter->drawRoundedRect(0,1,w,h - 2, 1.0, 1.0);

    if(state == 1)
    {
        // paint glow
        QRadialGradient gradient(50, 50, 50, 50, 50);
        gradient.setColorAt(0, QColor(255, 255, 255, 255));
        gradient.setColorAt(1, QColor(255, 255, 255, 128));
        QBrush brushGlow(gradient);

        painter->setBrush(gradient);
        painter->setPen(Qt::NoPen);
        painter->drawRoundedRect(-1,-1,w+1,h+1, 1.0, 1.0);
    }
}
