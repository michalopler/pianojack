#ifndef MUSICXMLLOADER_HPP
#define MUSICXMLLOADER_HPP

#include <QString>
#include <QXmlStreamReader>
#include <QFile>
#include <QList>
#include <map>
#include <vector>

// Simple struct for storing notes
struct XmlNote
{
    XmlNote(int v, int b, int e): begin(b), end(e), val(v) {}
    int begin;
    int end;
    int val;
};

// XmlLoader
// loads MusicXML files using QXmlStreamReader
class XmlLoader
{
public:
    XmlLoader(const QString& fileName);
    ~XmlLoader();

    void getParts(QList<QString>& partList);
    void readPart(const QString& partName, double t, std::vector<XmlNote>& notes);


private:
    void readHeader();
    bool moveToNext(const QString& name, const QString& parent);

    bool endOf(const QString& e);
    bool beginOf(const QString& e);

    int getStep(const char ch);

    QXmlStreamReader* xml;
    QFile* file;
    std::map<QString, QString> parts;
};


#endif // MUSICXMLLOADER_HPP
