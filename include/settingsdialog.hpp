#ifndef SETTINGSDIALOG_HPP
#define SETTINGSDIALOG_HPP

#include <vector>
#include <string>
#include <QString>
#include "ui_settings.h"

// SettingsDialog
// QDialog with app settings
class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsDialog (QWidget *parent = 0);
    void setVelocity(int);
    void setTranspose(int);
    void setSpeed(int);
    void setVisibility(bool);
    void setInPorts(std::vector<std::string>, std::string);
    void setOutPorts(std::vector<std::string>, std::string);

    int getVelocity();
    int getTranspose();
    int getSpeed();
    bool getVisibility();
    std::string getOutPort();
    std::string getInPort();

private:
    Ui::SettingsDialog ui;
};

#endif // SETTINGSDIALOG_HPP
