#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

#include <QObject>
#include <QDebug>
#include <QVBoxLayout>
#include <QMainWindow>
#include <QMenu>

#include <string>

#include "midihandler.hpp"
#include "pianokeyboard.hpp"
#include "settingsdialog.hpp"
#include "gameview.hpp"
#include "musicxmlloader.hpp"

// GameManager
// Does most of the logic inside PianoJACK
//  - redirecting MIDI signals, handling file actions and so on.
class GameManager : public QMainWindow, public MidiClass
{
    Q_OBJECT

public:
    GameManager();
    ~GameManager();
    void activate();

    bool processMidiInEvent(MidiEvent&);
    void jackError(std::string);

public slots:
    void noteOn(int value);
    void noteOff(int value);
    void sustainPedal(bool value);

private slots:
    void about();
    void settings();
    void fileOpen();
    void endWithError(QString);

signals:
    void midiNoteOn(int value);
    void midiNoteOff(int value);
    void raiseError(QString);

protected:
     void closeEvent(QCloseEvent *event);

private:
    void connectSignals();
    void readSettings();
    void writeSettings();
    void load(const QString& name);
    int transposeNote(int);


    MidiHandler midi;
    int inChannel;

    QVBoxLayout* layout;
    QWidget* window;
    PianoKeyboard* keyboard;
    GameView* gameWidget;
    SettingsDialog* setDialog;

    int velocity;
    int transpose;
    int speed;
    bool gameVisible;
};

#endif // GAMEMANAGER_HPP
