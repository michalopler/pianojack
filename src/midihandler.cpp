#include "midihandler.hpp"
#include "gamemanager.hpp"

#include <QDebug>
#include <string.h>

using namespace std;

MidiHandler::MidiHandler(MidiClass* p): outBuffer(1000), client(NULL), outPort(NULL),
    inPort(NULL), parent(p), velocity(64){}

MidiHandler::~MidiHandler() {
    if(client != NULL) jack_client_close(client);
}

// startup function
void MidiHandler::activate() {
    if ((client = jack_client_open("PianoJACK", JackNoStartServer , NULL)) == 0) {
        parent->jackError("Failed to initialize JACK client. Run jackd to start JACK server");
        return;
    }

    outPort  = jack_port_register (client, "midi_out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
    inPort  = jack_port_register (client, "midi_in", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);

    jack_set_process_callback (client, staticProcess, static_cast<void*>(this));
    jack_on_shutdown(client,staticShutDown,  static_cast<void*>(this));

    if (jack_activate(client) != 0)
    {
        parent->jackError("JACK client failed to activate. Something wrong with JACK server.");
    }
}

// returns vector of available in ports - output MIDI ports
std::vector<std::string> MidiHandler::getInPorts()
{
    const char** ports = jack_get_ports(client, NULL, JACK_DEFAULT_MIDI_TYPE,  JackPortIsOutput);
    std::vector<std::string> inPorts;
    while(ports != NULL && *ports != NULL)
    {
        // exclude our own ports
        if(strncmp(*ports,"PianoJACK:", 10) != 0) inPorts.push_back(*ports);
        ports++;
    }
    return inPorts;
}

// returns vector of available out ports - input MIDI ports
std::vector<std::string> MidiHandler::getOutPorts()
{
    const char** ports = jack_get_ports(client, NULL, JACK_DEFAULT_MIDI_TYPE,  JackPortIsInput);
    std::vector<std::string> outPorts;
    while(ports != NULL && *ports != NULL)
    {
        // exclude our own ports
        if(strncmp(*ports,"PianoJACK:", 10) != 0) outPorts.push_back(*ports);
        ports++;
    }
    return outPorts;
}

// returns vector of ports connected to input - these are in fact output ports
std::vector<std::string> MidiHandler::getConnectedInPorts()
{
    const char** ports = jack_port_get_all_connections(client, inPort);
    std::vector<std::string> inPorts;
    while(ports != NULL && *ports != NULL)
    {
        inPorts.push_back(*ports);
        ports++;
    }
    return inPorts;
}

// returns vector of connectedoutput MIDI ports
std::vector<std::string> MidiHandler::getConnectedOutPorts()
{
    const char** ports = jack_port_get_all_connections(client, outPort);
    std::vector<std::string> outPorts;
    while(ports != NULL && *ports != NULL)
    {
        outPorts.push_back(*ports);
        ports++;
    }
    return outPorts;
}

// conects MIDI input
bool MidiHandler::connectInPort(const std::string& name)
{
    if(jack_connect(client,name.c_str(),"PianoJACK:midi_in")!=0) return false;
    else return true;
}

// connects MIDI output
bool MidiHandler::connectOutPort(const std::string& name)
{
    if(jack_connect(client,"PianoJACK:midi_out",name.c_str())!=0) return false;
    else return true;
}

// static method called from JACK Audio, just proxy to member method
int MidiHandler::staticProcess(jack_nframes_t nframes, void *arg)
{
  return static_cast<MidiHandler*>(arg)->process(nframes);
}

// static callback from JACK
void MidiHandler::staticShutDown(void* arg)
{
    static_cast<MidiHandler*>(arg)->parent->jackError("JACK server was shut down.\n"
                                    "Please start it before launching Piano JACK again");
}

// processing MIDI data from JACK
int MidiHandler::process(jack_nframes_t nframes)
{

    // TODO zpracovani inputu
    void* outPortBuf = jack_port_get_buffer( outPort, nframes );
    void* inPortBuf = jack_port_get_buffer( inPort, nframes );

    // clear output buffer
    jack_midi_clear_buffer(outPortBuf);


    // get frame time from the JACK server
    jack_nframes_t t = jack_last_frame_time(client);

    jack_nframes_t eventCount = jack_midi_get_event_count(inPortBuf);

    std::vector<MidiEvent> eventVector;
    jack_midi_event_t inEvent;

    // get MIDI input from inPort
    if(eventCount > 0)
    {
        for(unsigned int i=0; i < eventCount; i++)
        {
            jack_midi_event_get(&inEvent, inPortBuf, i);
            MidiEvent e(inEvent);
            if(parent->processMidiInEvent(e)) eventVector.push_back(e);
        }
    }

    MidiEvent outEvent;

    // write MIDI output from buffer
    while(outBuffer.peek(outEvent) == 0  && outEvent.frame < t + nframes) {
        jack_midi_data_t* buffer = jack_midi_event_reserve( outPortBuf, 0, outEvent.size);
        if(buffer == 0) {
            // this is not critical, co just silently ignore
            continue;
        }
        else {
            memcpy( buffer, outEvent.data, sizeof(char) *outEvent.size );
            outBuffer.readAdvance();
        }
    }

    // and append data from input
    for(auto eventIt = eventVector.begin(); eventIt != eventVector.end() &&
        eventIt->frame < t + nframes; ++eventIt) {
        jack_midi_data_t* buffer = jack_midi_event_reserve( outPortBuf, eventIt->frame, eventIt->size);
        if(buffer == 0) {
            // this is not critical, co just silently ignore
            continue;
        }
        memcpy(buffer, eventIt->data, sizeof(char) *eventIt->size);
    }

    return 0;
}

