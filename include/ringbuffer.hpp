#ifndef RINGBUFFER_HPP
#define RINGBUFFER_HPP

#include <string.h>

extern "C" {
#include <jack/ringbuffer.h>
}

// RingBuffer
// C+++ wrapper around JACK Audio lock free container useful for storing MIDI data
template<typename Type> class RingBuffer {
private:
	size_t size_;
	jack_ringbuffer_t* rbPtr_;
public:
	RingBuffer(size_t size): size_(size), rbPtr_(nullptr){
		rbPtr_ = jack_ringbuffer_create(size_ * sizeof(Type));
	}
	
	~RingBuffer(){
		if(rbPtr_ != nullptr)
			jack_ringbuffer_free(rbPtr_);
	}
	
	// returns size of ring buffer
	size_t length() {return size_;}
	
	// number of Type items that can be read
	size_t getReadSpace(){ 
		return jack_ringbuffer_read_space(rbPtr_) / sizeof(Type);
	}
	
	// number of Type items that can be written
	size_t getWriteSpace(){
		return jack_ringbuffer_write_space(rbPtr_) / sizeof(Type);
	}
	
	// read array of items
	int read(Type* dest, unsigned int count) {
		jack_ringbuffer_data_t readInfo[2];
		unsigned int readSize = count * sizeof(Type);
		// TODO throw error
		if(getReadSpace()  <= 0) return 1;

		jack_ringbuffer_get_read_vector(rbPtr_, readInfo);

		if(readInfo[0].len >= readSize) {
			memcpy(dest, readInfo[0].buf, readSize);
		}
		else {
			if(readInfo[0].len == 0) {
				memcpy(dest, readInfo[1].buf, readSize);
			}
			else {
				char* bt = (char*) dest;
				memcpy(bt, readInfo[0].buf, readInfo[0].len);
				memcpy(bt + readInfo[0].len, readInfo[1].buf, readSize - readInfo[0].len);
			}
		}
		jack_ringbuffer_read_advance(rbPtr_, readSize);
		return 0;
	}

	// read item
	int read(Type& dest) {
        if(getReadSpace() <= 0) {
			return 1;
		}
		jack_ringbuffer_read(rbPtr_, (char*) &dest, sizeof(Type));
		return 0;
	}
	
	// peek - read without moving position
	int peek(Type& dest) {
        if(getReadSpace() <= 0) {
			return 1;
		}
		jack_ringbuffer_peek(rbPtr_, (char*) &dest, sizeof(Type));
		return 0;
	}

	// move cursor without reading
	void readAdvance() {
		jack_ringbuffer_read_advance(rbPtr_, sizeof(Type));
	}

	// write array of items
	int write(Type* src, unsigned int count) {
		jack_ringbuffer_data_t writeInfo[2];
		unsigned int writeSize = count * sizeof(Type);
        if(count > getWriteSpace()) {
			return 1;
		}

		jack_ringbuffer_get_write_vector(rbPtr_, writeInfo);

		if(writeInfo[0].len >= writeSize) {
			memcpy(writeInfo[0].buf, src, writeSize);
		}
		else {	
			if(writeInfo[0].len == 0) {
				memcpy(writeInfo[1].buf, src, writeSize);
			}
			else {
				char* bt = (char*) src;
				memcpy(writeInfo[0].buf, bt, writeInfo[0].len);
				memcpy(writeInfo[1].buf, bt + writeInfo[0].len, writeSize - writeInfo[0].len);
			}
		}
		jack_ringbuffer_write_advance(rbPtr_, writeSize);
		return 0;
	}

	// write single item
	int write(Type& src) {
        if(getWriteSpace() <= 0){
			return 1;
		}
		jack_ringbuffer_write(rbPtr_, (char*) &src, sizeof(Type));
		return 0;
	}
};

#endif
