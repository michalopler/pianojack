#include "settingsdialog.hpp"
#include "algorithm"
#include <QDebug>

SettingsDialog::SettingsDialog(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}

// setters
void SettingsDialog::setVelocity(int value)
{
    ui.velocitySlide->setValue(value);
}

void SettingsDialog::setTranspose(int value)
{
    ui.transposeSpin->setValue(value);
}

void SettingsDialog::setSpeed(int value)
{
    ui.speedSlide->setValue(value);
}

void SettingsDialog::setVisibility(bool value)
{
    if(value) ui.visibleCheck->setCheckState(Qt::Checked);
    else ui.visibleCheck->setCheckState(Qt::Unchecked);
}

void SettingsDialog::setInPorts(std::vector<std::string> inPorts, std::string connected)
{
    ui.inMidiPick->clear();

    // some lambda for fun
    for_each(inPorts.begin(), inPorts.end(), [&](std::string& s) {
             ui.inMidiPick->addItem(s.c_str());
    });
    ui.inMidiPick->addItem("none");

    int index = ui.inMidiPick->findText(connected.c_str());
    if ( index != -1 ) {
       ui.inMidiPick->setCurrentIndex(index);
    }
}

void SettingsDialog::setOutPorts(std::vector<std::string> outPorts, std::string connected)
{
    ui.outMidiPick->clear();

    // some lambda for fun
    for_each(outPorts.begin(), outPorts.end(), [&](std::string& s) {
            ui.outMidiPick->addItem(s.c_str());});
    ui.outMidiPick->addItem("none");

    int index = ui.outMidiPick->findText(connected.c_str());
    if ( index != -1 ) {
       ui.outMidiPick->setCurrentIndex(index);
    }
}

// getters
std::string SettingsDialog::getInPort()
{
    return ui.inMidiPick->currentText().toStdString();
}

std::string SettingsDialog::getOutPort()
{
    return ui.outMidiPick->currentText().toStdString();
}

int SettingsDialog::getVelocity()
{
    return ui.velocitySlide->value();
}

int SettingsDialog::getTranspose()
{
    return ui.transposeSpin->value();
}

int SettingsDialog::getSpeed()
{
    return ui.speedSlide->value();
}

bool SettingsDialog::getVisibility()
{
    return ui.visibleCheck->isChecked();
}
