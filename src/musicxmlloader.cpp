#include <stdexcept>
#include <QDebug>
#include <algorithm>

#include "musicxmlloader.hpp"


XmlLoader::XmlLoader(const QString& fileName)
{
    file = new QFile(fileName);
    if(file->open(QIODevice::ReadOnly)) {
        xml = new QXmlStreamReader(file);
    }
    else throw std::invalid_argument("wrong filename");

    readHeader();
}

XmlLoader::~XmlLoader()
{
    file->close();
    delete xml;
    delete file;
}


// parse header
void XmlLoader::readHeader()
{
    moveToNext("part-list", "score");
    while(moveToNext("score-part", "part-list"))
    {
        QString id = xml->attributes().value("id").toString();
        if(moveToNext("part-name", "score-part"))
        {
            parts.emplace(xml->readElementText(), id);
        }
    }
}

// parse parts and set durations to t ticks per quarter note
void XmlLoader::readPart(const QString &partName, double t, std::vector<XmlNote>& notes)
{
    QString id = parts[partName];
    while(moveToNext("part", "score") && xml->attributes().value("id") != id);
    if(xml->attributes().value("id") != id) throw std::invalid_argument("xml error");
    moveToNext("measure", "part");

    int divs = 1;
    int time = 0;
    int prevTime = 0;

    bool inNote = false;

    bool rest = false;
    bool chord = false;
    bool tiestop = false;
    bool tiestart = false;

    int step = 0;
    int duration = 0;

    std::vector<XmlNote> tiedNotes;

    // get the notes and set durations
    while (!xml->atEnd())  {
        xml->readNext();

        // end of part
        if(endOf("part")) break;

        // change of divisions
        if(beginOf("attributes"))
        {
            if(moveToNext("divisions", "attributes")) divs = xml->readElementText().toInt();
        }

        // note itself
        if(beginOf("note")) inNote = true;

        // various describing properties
        if(beginOf("duration") && inNote) duration = xml->readElementText().toInt();
        if(beginOf("octave") && inNote) step += 12 * xml->readElementText().toInt();
        if(beginOf("step") && inNote) step += getStep(xml->readElementText().at(0).toAscii());
        if(beginOf("alter") && inNote) step += xml->readElementText().toInt();
        if(beginOf("rest") && inNote) rest = true;
        if(beginOf("tie") && inNote && xml->attributes().value("type").toString() == "stop") tiestop = true;
        if(beginOf("tie") && inNote && xml->attributes().value("type").toString() == "start") tiestart = true;

        if(beginOf("chord") && inNote) chord = true;

        if(beginOf("backup"))
        {
            if(moveToNext("duration", "backup"))
                time -= xml->readElementText().toInt() * t / divs;
        }

        if(beginOf("forward"))
        {
            if(moveToNext("duration", "forward"))
                time += xml->readElementText().toInt() * t / divs;
        }

        if(endOf("note") && inNote)
        {
            if(!rest)
            {
                XmlNote note(0,0,0);

                // if this is continuation of tie look in the opened notes
                if(tiestop)
                {
                    auto i = tiedNotes.begin();
                    for( ;i != tiedNotes.end(); ++i)
                        if(step == i->val) break;
                    if(i == tiedNotes.end()) tiestop = false;
                    else
                    {
                        note = *i;
                        tiedNotes.erase(i);
                    }
                }

                // else set new note
                if(!tiestop)
                {
                    note.val = step;
                    if(!chord) note.begin = time;
                    else note.begin = prevTime;
                }

                if(!chord) note.end = time + duration * t / divs;
                else note.end = prevTime + duration * t / divs;

                if(tiestart)
                    tiedNotes.push_back(note);
                else
                    notes.push_back(note);
            }

            if(!chord)
            {
                prevTime = time;
                time += duration * t / divs;
            }

            // reset flags
            rest = false;
            inNote = false;
            chord = false;
            tiestop = false;
            tiestart = false;

            step = 0;
            duration = 0;
        }
    }

    // sort notes by their begins
    std::sort(notes.begin(), notes.end(),[](const XmlNote& a, const XmlNote& b) { return a.begin < b.begin;});

}

// get parts
void XmlLoader::getParts(QList<QString> &partList)
{
    for(auto i = parts.begin(); i != parts.end(); ++i)
    {
        partList.push_back(i->first);
    }
}

// helper function, tests end od element
bool XmlLoader::endOf(const QString& e)
{
    if(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == e) return true;
    else return false;
}

// helper function, tests begin od element
bool XmlLoader::beginOf(const QString& e)
{
    if(xml->tokenType() == QXmlStreamReader::StartElement && xml->name() == e) return true;
    else return false;
}

// convert step to midi event
int XmlLoader::getStep(const char ch)
{
    switch(ch)
    {
        case 'A':
            return 21;
        case 'B':
            return 23;
        case 'C':
            return 12;
        case 'D':
            return 14;
        case 'E':
            return 16;
        case 'F':
            return 17;
        case 'G':
            return 19;
        default:
            return 0;
    }
}

// move to next element with name or to the end of parent
bool XmlLoader::moveToNext(const QString& name, const QString& parent)
{
    while (!xml->atEnd())  {
        xml->readNext();
        if(beginOf(name)) return true;
        if(endOf(parent)) return false;
    }
    throw std::invalid_argument("xml error");
}
